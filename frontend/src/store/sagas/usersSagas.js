import {put, takeEvery} from "redux-saga/effects";
import {
    googleLogin,
    registerUser,
    registerUserFailure,
    registerUserSuccess,
    signIn,
    signInFailure,
    signInSuccess,
    userLogout, userLogoutSuccess
} from "../actions/usersActions";
import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";
import {toast} from "react-toastify";
import {GOOGLE_LOGIN, USERS_SESSIONS_URL, USERS_URL} from "../../config";

export function* registerUserSaga({payload: userData}) {
    try {
        const {data} = yield axiosApi.post(USERS_URL, userData);
        yield put(registerUserSuccess(data));
        yield put(historyPush('/'));
        toast.success('Created successful !');
    } catch (err) {
        if (!err.response) toast.error( err.message);
        yield put(registerUserFailure(err.response?.data));
    }
}

export function* signInSaga({payload: userData}) {
    try {
        const {data} = yield axiosApi.post(USERS_SESSIONS_URL, userData);
        yield put(signInSuccess(data));
        yield put(historyPush('/'));
        toast.success('Login successful !');
    } catch (err) {
        if (!err.response) toast.error( err.message);
        yield put(signInFailure(err.response?.data));
    }
}

export function* googleLoginSaga({payload: googleData}) {
    try {
        const {data} = yield axiosApi.post(GOOGLE_LOGIN, {token: googleData.tokenId});
        yield put(signInSuccess(data));
        yield put(historyPush('/'));
        toast.success('Login successful !');
    } catch (err) {
        toast.error(err.response?.data?.global || err.message);
        yield put(signInFailure(err.response?.data));
    }
}

export function* userLogoutSaga() {
    try {
        yield axiosApi.delete(USERS_SESSIONS_URL);
        yield put(userLogoutSuccess());
        yield put(historyPush('/'));
    } catch (err) {
        toast.error(err.response?.data?.global || err.message);
        yield put(signInFailure(err.response?.data));
    }
}

const usersSaga = [
    takeEvery(registerUser, registerUserSaga),
    takeEvery(signIn, signInSaga),
    takeEvery(googleLogin, googleLoginSaga),
    takeEvery(userLogout, userLogoutSaga),
];

export default usersSaga;