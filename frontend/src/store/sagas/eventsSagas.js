import {put, takeEvery} from "redux-saga/effects";
import {toast} from "react-toastify";
import {
    addEvent,
    addEventFailure,
    addEventSuccess,
    deleteEvent,
    deleteEventFailure,
    deleteEventSuccess,
    fetchEvents,
    fetchEventsSuccess, inviteDelete, inviteDeleteFailure, inviteDeleteSuccess,
    inviteEvent,
    inviteEventFailure,
    inviteEventSuccess
} from "../actions/eventsActions";
import axiosApi from "../../axiosApi";
import {EVENTS_INVITE_URL, EVENTS_URL} from "../../config";

export function* fetchEventsSaga() {
    try {
        const {data} = yield axiosApi.get(EVENTS_URL);
        yield put(fetchEventsSuccess(data));
    } catch (err) {
        if (!err.response) toast.error(err.message);
        toast.warning('Please login first to view events')
        yield put(fetchEventsSuccess(err.response?.data));
    }
}

export function* addEventSaga({payload: newEvent}) {
    try {
        const {data} = yield axiosApi.post(EVENTS_URL, newEvent.data);
        yield put(addEventSuccess(data));
        newEvent.successCallback();
        toast.success('Event successful created');
    } catch (err) {
        if (!err.response) toast.error(err.message);
        yield put(addEventFailure(err.response?.data));
    }
}

export function* deleteEventSaga({payload: event_id}) {
    try {
        yield axiosApi.delete(EVENTS_URL, {data: {event_id}});
        yield put(deleteEventSuccess(event_id));
        toast.success('Event successful deleted');
    } catch (err) {
        if (!err.response) {
            toast.error(err.message);
        } else {
            toast.error(err.response?.data?.global);
        }
        yield put(deleteEventFailure(err.response?.data));
    }
}

export function* inviteEventSaga({payload: invite}) {
    try {
        yield axiosApi.post(EVENTS_INVITE_URL, invite.data);
        yield put(fetchEvents());
        yield put(inviteEventSuccess());
        invite.successCallback();
        toast.success('Invite successful created');
    } catch (err) {
        if (!err.response) {
            toast.error(err.message);
        } else {
            toast.error(err.response?.data?.global);
        }
        yield put(inviteEventFailure(err.response?.data));
    }
}

export function* inviteDeleteSaga({payload: invite}) {
    try {
        yield axiosApi.delete(EVENTS_INVITE_URL, {data: invite});
        yield put(fetchEvents());
        yield put(inviteDeleteSuccess());
        toast.success('Invite successful deleted');
    } catch (err) {
        if (!err.response) {
            toast.error(err.message);
        } else {
            toast.error(err.response?.data?.global);
        }
        yield put(inviteDeleteFailure(err.response?.data));
    }
}


const eventsSaga = [
    takeEvery(fetchEvents, fetchEventsSaga),
    takeEvery(addEvent, addEventSaga),
    takeEvery(deleteEvent, deleteEventSaga),
    takeEvery(inviteEvent, inviteEventSaga),
    takeEvery(inviteDelete, inviteDeleteSaga),
];

export default eventsSaga;