import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    data: null,
    errLogIn: null,
    errAccount: null,
    registerLoading: false,
    loginLoading: false,
};

const name = 'profile';

const usersSlice = createSlice({
    name,
    initialState,
    reducers: {
        registerUser(state) {
            state.registerLoading = true;
            state.errAccount = null;
        },
        registerUserSuccess(state, {payload: userData}) {
            state.data = userData;
            state.registerLoading = false;
        },
        registerUserFailure(state, {payload}) {
            state.registerLoading = false;
            state.errAccount = payload;
        },
        signIn(state) {
            state.loginLoading = true;
            state.errLogIn = null;
        },
        googleLogin(state) {
            state.loginLoading = true;
            state.errLogIn = null;
        },
        signInSuccess(state, {payload}) {
            state.loginLoading = false;
            state.data = payload;
        },
        signInFailure(state, {payload}) {
            state.loginLoading = false;
            state.errLogIn = payload;
        },
        userLogout() {
        },
        userLogoutSuccess(state) {
            state.data = null;
        },
    }
});

export default usersSlice;