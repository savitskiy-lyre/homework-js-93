import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    data: null,
    errAdding: null,
    errDeleting: null,
    errInviting: null,
    errFetching: null,
    addLoading: false,
    inviteLoading: false,
    inviteDeleteLoading: false,
    deleteLoading: false,
};

const name = 'events';

const eventsSlice = createSlice({
    name,
    initialState,
    reducers: {
        fetchEvents(state) {
            state.errFetching = null;
        },
        fetchEventsSuccess(state, {payload}) {
            state.data = payload;
        },
        fetchEventsFailure(state, {payload}) {
            state.errFetching = payload;
        },
        addEvent(state) {
            state.addLoading = true;
            state.errAdding = null;
        },
        addEventSuccess(state, {payload}) {
            state.addLoading = false;
            state.data = [...state.data, payload];
        },
        addEventFailure(state, {payload}) {
            state.addLoading = false;
            state.errAdding = payload;
        },
        deleteEvent(state, {payload: event_id}) {
            state.deleteLoading = event_id;
            state.errDeleting = null;
        },
        deleteEventSuccess(state, {payload: event_id}) {
            state.deleteLoading = false;
            state.data = state.data.filter((a)=>{return a._id !== event_id});
        },
        deleteEventFailure(state, {payload}) {
            state.deleteLoading = false;
            state.errDeleting = payload;
        },
        inviteEvent(state) {
            state.inviteLoading = true;
            state.errInviting = null;
        },
        inviteEventSuccess(state) {
            state.inviteLoading = false;
        },
        inviteEventFailure(state, {payload}) {
            state.inviteLoading = false;
            state.errInviting = payload;
        },
        inviteDelete(state, {payload: invite}) {
            state.inviteDeleteLoading = invite.email;
            state.errInviting = null;
        },
        inviteDeleteSuccess(state) {
            state.inviteDeleteLoading = false;
        },
        inviteDeleteFailure(state, {payload}) {
            state.inviteDeleteLoading = false;
            state.errInviting = payload;
        },
    }
});

export default eventsSlice;