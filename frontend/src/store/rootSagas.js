import {all} from 'redux-saga/effects';
import usersSaga from "./sagas/usersSagas";
import eventsSaga from "./sagas/eventsSagas";

export function* rootSagas() {
    yield all([
        ...usersSaga,
        ...eventsSaga
    ])
}