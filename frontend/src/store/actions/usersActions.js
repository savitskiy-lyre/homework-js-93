import usersSlice from "../slices/usersSlice";

export const {
    registerUser,
    registerUserSuccess,
    registerUserFailure,
    signIn,
    googleLogin,
    signInSuccess,
    signInFailure,
    userLogout,
    userLogoutSuccess,

} = usersSlice.actions;