import eventsSlice from "../slices/eventsSlice";

export const {
    fetchEvents,
    fetchEventsSuccess,
    fetchEventsFailure,
    addEvent,
    addEventSuccess,
    addEventFailure,
    deleteEvent,
    deleteEventSuccess,
    deleteEventFailure,
    inviteEvent,
    inviteEventSuccess,
    inviteEventFailure,
    inviteDelete,
    inviteDeleteSuccess,
    inviteDeleteFailure,

} = eventsSlice.actions;