import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import SignIn from "./containers/SignIn";
import SignUp from "./containers/SignUp";
import Events from "./containers/Events/Events";

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Events}/>
                <Route path="/signin" component={SignIn}/>
                <Route path="/signup" component={SignUp}/>
            </Switch>
        </Layout>
    );
};

export default App;
