import React, {useEffect} from 'react';
import {Stack} from "@mui/material";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import {useDispatch, useSelector} from "react-redux";
import {addEvent, deleteEvent, fetchEvents} from "../../store/actions/eventsActions";
import AddEventModalBtn from "../../components/AddEventModalBtn/AddEventModalBtn";
import EventsSkeleton from "../../components/UI/EventsSkeleton/EventsSkeleton";
import EventRow from "./EventRow/EventRow";
import InviteEventModalBtn from "../../components/InviteEventModalBtn/InviteEventModalBtn";
import InvitationsModalBtn from "../../components/InvitationsModalBtn/InvitationsModalBtn";


const Events = () => {
    const dispatch = useDispatch();
    const eventsData = useSelector(state => state.events.data);
    const deleteLoadingId = useSelector(state => state.events.deleteLoading);
    const profile = useSelector(state => state.profile.data);

    useEffect(() => {
        dispatch(fetchEvents());
    }, [dispatch]);

    return (
        <Stack mt={2} p={1} border={'2px solid gainsboro'} borderRadius={'6px'} minHeight={'90vh'}>
            <Stack borderBottom={'1px solid gainsboro'} py={1} mb={3}>
                <Grid container spacing={2} flexWrap={'nowrap'} justifyContent={"space-between"}>
                    <Grid item>
                        <Typography variant={'h4'}>
                            Events list:
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Grid container flexWrap={"nowrap"} spacing={2}>
                            <Grid item>
                                <AddEventModalBtn
                                    onSubmit={(eventData) => {
                                        dispatch(addEvent(eventData))
                                    }}
                                    title={'Add'}
                                />
                            </Grid>
                            <Grid item>
                                <InviteEventModalBtn
                                    title={'Invite'}
                                />
                            </Grid>
                            <Grid item>
                                <InvitationsModalBtn/>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Stack>
            {!eventsData && (
                <EventsSkeleton/>
            )}
            {eventsData?.length > 0 && (
                eventsData.map((event) => {
                    return (
                        <EventRow
                            key={event._id}
                            date={event.date}
                            message={event.message}
                            duration={event.duration}
                            disallowDeletion={profile?._id !== event.author._id}
                            onDelete={() => {
                                dispatch(deleteEvent(event._id))
                            }}
                            inProgress={deleteLoadingId === event._id}
                        />
                    );
                })
            )}
        </Stack>
    );
};

export default Events;