import React from 'react';
import Grid from "@mui/material/Grid";
import {CircularProgress, IconButton, Paper, Typography} from "@mui/material";
import ClearIcon from '@mui/icons-material/Clear';

const EventRow = ({date, message, duration, disallowDeletion, inProgress, onDelete}) => {
    return (
        <Grid container justifyContent={"space-between"} spacing={1} component={Paper} mb={2} alignItems={"center"} minHeight={'70px'}>
            <Grid item xs={4}>
                <Typography variant={'body1'}>
                    <strong>{date}</strong>
                </Typography>
            </Grid>
            <Grid item xs={6}>
                <Typography variant={'body1'}>
                    {message}
                </Typography>
            </Grid>
            <Grid item xs={1}>
                <Typography variant={'subtitle1'}>
                    {duration} hours
                </Typography>
            </Grid>
            <Grid item xs={1}>
                <IconButton color={"error"} disabled={disallowDeletion || inProgress} onClick={onDelete}>
                    {inProgress ?  (
                        <CircularProgress/>
                    ) : (
                        <ClearIcon/>
                    )}

                </IconButton>
            </Grid>
        </Grid>
    );
};

export default EventRow;