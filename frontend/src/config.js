export const BASE_URL = 'http://localhost:9000';
export const USERS_URL = '/users';
export const USERS_SESSIONS_URL = '/users/sessions';
export const GOOGLE_LOGIN = '/users/googleLogin';
export const EVENTS_URL = '/events';
export const EVENTS_INVITE_URL = '/events/invite';
