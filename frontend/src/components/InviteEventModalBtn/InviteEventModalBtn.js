import React, {useState} from 'react';
import {Button, FormControl, InputLabel, MenuItem, Modal, Select, Stack} from "@mui/material";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import {LoadingButton} from "@mui/lab";
import {useDispatch, useSelector} from "react-redux";
import Grid from "@mui/material/Grid";
import {inviteEvent} from "../../store/actions/eventsActions";

const styleInviteForm = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const initState = {
    email: '',
    event_id: '',
};

const InviteEventModalBtn = ({title}) => {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.events.inviteLoading);
    const errors = useSelector(state => state.events.errInviting);
    const [isOpenAddForm, setIsOpenAddForm] = useState(false);
    const [state, setState] = useState(initState);
    const profile = useSelector(state => state.profile.data);
    const events = useSelector(state => state.events.data);
    let eventsOptions = null;
    if (events?.length > 0) {
        eventsOptions = events.filter((e) => {
            return e.author._id === profile?._id
        });
    }

    const openInviteForm = () => setIsOpenAddForm(true);
    const closeInviteForm = () => setIsOpenAddForm(false);

    const inpChanger = (e) => {
        setState(prevState => ({...prevState, [e.target.name]: e.target.value}));
    };

    const inviteEventSubmit = (e) => {
        e.preventDefault();
        dispatch(inviteEvent({
            data: state,
            successCallback: () => {
                setState(initState);
                closeInviteForm();
            }
        }))
    }
    return (
        <>
            <Button onClick={openInviteForm} variant={"contained"}>{title}</Button>
            <Modal
                open={isOpenAddForm}
                onClose={closeInviteForm}
            >
                <Box sx={styleInviteForm}>
                    <Stack component={'form'} onSubmit={inviteEventSubmit} spacing={2}>
                        <Typography variant="h6" component="h2" textAlign={"center"}>
                            Invite
                        </Typography>
                        <TextField
                            label="Email"
                            name="email"
                            value={state.email}
                            onChange={inpChanger}
                            error={Boolean(errors?.email)}
                            helperText={errors?.email?.message}
                            autoFocus
                        />
                        <FormControl fullWidth>
                            <InputLabel>Event</InputLabel>
                            <Select
                                value={state.event_id}
                                label="Event"
                                name="event_id"
                                onChange={inpChanger}
                            >
                                {eventsOptions && (
                                    eventsOptions.map((event) => {
                                        return (
                                            <MenuItem value={event._id}
                                                      key={event._id + 'option'}
                                            >
                                                <Grid container justifyContent={"space-between"}>
                                                    <Grid item>
                                                        <strong style={{paddingRight: '5px'}}>{event.message} </strong>
                                                    </Grid>
                                                    <Grid item>
                                                        <Typography variant={"body2"}>{event.date}</Typography>
                                                    </Grid>
                                                </Grid>
                                            </MenuItem>
                                        );
                                    })
                                )}
                            </Select>
                        </FormControl>
                        <LoadingButton
                            loading={loading}
                            type="submit"
                            variant="contained"
                            fullWidth
                        >
                            Submit
                        </LoadingButton>
                    </Stack>
                </Box>
            </Modal>
        </>
    );
};

export default InviteEventModalBtn;