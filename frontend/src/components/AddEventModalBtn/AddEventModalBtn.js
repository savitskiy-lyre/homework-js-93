import React, {useCallback, useState} from 'react';
import {Button, Modal, Stack} from "@mui/material";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import {useSelector} from "react-redux";
import {LoadingButton} from "@mui/lab";

const styleAddForm = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const initState = {
    message: '',
    date: '',
    duration: '',
};

const AddEventModalBtn = ({title, onSubmit}) => {
    const errors = useSelector(state => state.events.errAdding);
    const loading = useSelector(state => state.events.addLoading);
    const [isOpenAddForm, setIsOpenAddForm] = useState(false);
    const [state, setState] = useState(initState);
    const openAddForm = () => setIsOpenAddForm(true);
    const closeAddForm = () => setIsOpenAddForm(false);
    const closeAndClearModal = useCallback(() => {
        setState(initState);
        setIsOpenAddForm(false);
    }, []);

    const inpChanger = (e) => {
        setState(prevState => ({...prevState, [e.target.name]: e.target.value}));
    };

    const addEventSubmit = (e) => {
        e.preventDefault();
        onSubmit({data: state, successCallback: closeAndClearModal});
    }

    return (
        <>
            <Button onClick={openAddForm} variant={"contained"}>{title}</Button>
            <Modal
                open={isOpenAddForm}
                onClose={closeAddForm}
            >
                <Box sx={styleAddForm}>
                    <Stack component={'form'} onSubmit={addEventSubmit} spacing={2}>
                        <Typography variant="h6" component="h2" textAlign={"center"}>
                            Add event
                        </Typography>
                        <TextField
                            label="Event message"
                            name="message"
                            value={state.message}
                            onChange={inpChanger}
                            error={Boolean(errors?.message)}
                            helperText={errors?.message?.message}
                            autoFocus
                        />
                        <TextField
                            label="Date"
                            name="date"
                            value={state.date}
                            onChange={inpChanger}
                            error={Boolean(errors?.date)}
                            helperText={errors?.date?.message}
                        />
                        <TextField
                            label="Duration"
                            name="duration"
                            value={state.duration}
                            onChange={inpChanger}
                            error={Boolean(errors?.duration)}
                            helperText={errors?.duration?.message}
                        />

                        <LoadingButton
                            loading={loading}
                            type="submit"
                            variant="contained"
                            fullWidth
                        >
                            Submit
                        </LoadingButton>
                    </Stack>
                </Box>
            </Modal>
        </>
    );
};

export default AddEventModalBtn;