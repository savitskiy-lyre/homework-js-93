import React, {useState} from 'react';
import Box from "@mui/material/Box";
import {Divider, IconButton, ListItemIcon, MenuItem, Tooltip, Typography} from "@mui/material";
import Menu from "@mui/material/Menu";
import {Link as RouterLink} from "react-router-dom";
import {Logout} from "@mui/icons-material";

const paperStyles = {
    overflow: 'visible',
    filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
    mt: 1.5,
    '& .MuiAvatar-root': {
        width: 32,
        height: 32,
        ml: -0.5,
        mr: 1,
    },
    '&:before': {
        content: '""',
        display: 'block',
        position: 'absolute',
        top: 0,
        right: 14,
        width: 10,
        height: 10,
        bgcolor: 'background.paper',
        transform: 'translateY(-50%) rotate(45deg)',
        zIndex: 0,
    },
};

const ProfileMenu = ({handleLogout, username, locationTo, profileImg}) => {
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <Box sx={{display: 'flex', alignItems: 'center', textAlign: 'center'}}>
                <Typography>
                    {username}
                </Typography>
                <Tooltip title="Account settings">
                    <IconButton onClick={handleClick} size="small" sx={{
                        ml: 2,
                        background: `${profileImg} 50% 50%`,
                        backgroundSize: "cover",
                        width: 32,
                        height: 32,
                    }}/>
                </Tooltip>
            </Box>
            <Menu
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                onClick={handleClose}
                PaperProps={{
                    elevation: 0,
                    sx: paperStyles,
                }}
                transformOrigin={{horizontal: 'right', vertical: 'top'}}
                anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
            >
                <MenuItem component={RouterLink} to={locationTo}>
                    Something
                </MenuItem>
                <Divider/>
                <MenuItem onClick={handleLogout}>
                    <ListItemIcon>
                        <Logout fontSize="small"/>
                    </ListItemIcon>
                    Logout
                </MenuItem>
            </Menu>
        </>
    );
};

export default ProfileMenu;