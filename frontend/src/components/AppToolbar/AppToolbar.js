import React from 'react';
import {AppBar, Button, Divider, Grid, Stack, Toolbar} from "@mui/material";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import ProfileMenu from "../ProfileMenu/ProfileMenu";
import {BASE_URL} from "../../config";
import EmptyAvatar from "../../assets/images/empty_avatar.png";
import {userLogout} from "../../store/actions/usersActions";

const AppToolbar = () => {
    const profile = useSelector((state) => state.profile.data);
    const dispatch = useDispatch();

    return (
        <>
            <AppBar position={"fixed"} color={'customBlack'}>
                <Toolbar>
                    <Grid container justifyContent={"space-between"} alignItems={"center"}>
                        <Grid item>
                            <Stack flexDirection={'row'}>
                                <Button component={Link}
                                        to='/'
                                        sx={{color: "inherit"}}
                                >
                                    Events
                                </Button>
                            </Stack>
                        </Grid>
                        {profile ? (
                            <ProfileMenu
                                handleLogout={() => {
                                    dispatch(userLogout());
                                }}
                                profileImg={profile?.image ? /http/.test(profile.image) ? `url(${profile.image})` : `url(${BASE_URL + profile.image})` : `url(${EmptyAvatar})`}
                                username={profile.username}
                                locationTo={'/something'}
                            />
                        ) : (
                            <Grid display={"inline-flex"}>
                                <Grid item>
                                    <Button component={Link}
                                            to='/signin'
                                            sx={{color: "inherit"}}
                                    >
                                        Sign in
                                    </Button>
                                </Grid>
                                <Grid item>
                                    <Divider orientation="vertical" sx={{backgroundColor: 'gainsboro',}}/>
                                </Grid>
                                <Grid item>
                                    <Button component={Link}
                                            to='/signup'
                                            sx={{color: "inherit"}}
                                    >
                                        Sign up
                                    </Button>
                                </Grid>
                            </Grid>
                        )
                        }
                    </Grid>
                </Toolbar>
            </AppBar>
            <Toolbar/>
        </>
    );
};

export default AppToolbar;