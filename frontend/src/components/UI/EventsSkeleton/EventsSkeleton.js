import React from 'react';
import Box from "@mui/material/Box";
import {Skeleton} from "@mui/material";

const EventsSkeleton = () => {
    return (
        <Box>
            <Skeleton animation="wave" height={100} />
            <Skeleton animation="wave" height={100} />
            <Skeleton animation="wave" height={100} />
            <Skeleton animation="wave" height={100} />
            <Skeleton animation="wave" height={100} />
            <Skeleton animation="wave" height={100} />
            <Skeleton animation="wave" height={100} />
            <Skeleton animation="wave" height={100} />
            <Skeleton animation="wave" height={100} />
        </Box>
    );
};

export default EventsSkeleton;