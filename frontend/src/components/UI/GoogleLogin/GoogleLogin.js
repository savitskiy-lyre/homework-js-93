import React from 'react';
import GoogleLogin from "react-google-login";
import {Button} from "@mui/material";
import GoogleIcon from '@mui/icons-material/Google';
import {useDispatch} from "react-redux";
import {googleLogin} from "../../../store/actions/usersActions";

const GoogleLoginButton = () => {
    const dispatch = useDispatch();

    const responseGoogle = (response) => {
        dispatch(googleLogin(response));
    }

    return (
        <GoogleLogin
            clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
            render={renderProps => (
                <Button onClick={renderProps.onClick}
                        fullWidth
                        variant={"outlined"}
                        startIcon={<GoogleIcon/>}
                >
                    Log in via Google
                </Button>
            )}
            onSuccess={responseGoogle}
            cookiePolicy={'single_host_origin'}
        />
    );
};

export default GoogleLoginButton;