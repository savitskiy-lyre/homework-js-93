import * as React from 'react';
import {useState} from 'react';
import Avatar from '@mui/material/Avatar';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import VpnKeyIcon from '@mui/icons-material/VpnKey';
import {Link as RouterLink} from 'react-router-dom';
import GoogleLoginButton from "../GoogleLogin/GoogleLogin";
import {Stack} from "@mui/material";
import {LoadingButton} from "@mui/lab";

const initState = {
    username: '',
    password: '',
};

const FormRegister = ({actionName, helperLinkName, toLocation, onSubmit, errors, loading}) => {
    const [state, setState] = useState(initState);
    const handleSubmit = (event) => {
        event.preventDefault();
        onSubmit(state);
    };
    const handleInpChange = (e) => {
        setState(prevState => ({...prevState, [e.target.name]: e.target.value}));
    };

    return (
        <Stack
            alignItems={"center"}
            minHeight={1000}
        >
            <Avatar sx={{m: 1, bgcolor: 'primary.main'}}>
                <VpnKeyIcon/>
            </Avatar>
            <Typography component="h1" variant="h5">
                {actionName}
            </Typography>
            <Stack component="form" onSubmit={handleSubmit}
                   noValidate sx={{mt: 1}} spacing={2}
                   maxWidth={900} width={"100%"}
            >
                <TextField
                    label="Login"
                    name="username"
                    value={state.username}
                    onChange={handleInpChange}
                    error={Boolean(errors?.username)}
                    helperText={errors?.username?.message}
                    autoFocus
                    required
                />
                <TextField
                    name="password"
                    label="Password"
                    type="password"
                    error={Boolean(errors?.password)}
                    helperText={errors?.password?.message}
                    value={state.password}
                    onChange={handleInpChange}
                    required
                />
                <LoadingButton
                    loading={loading}
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{mt: 3, mb: 2}}
                >
                    {actionName}
                </LoadingButton>
                <GoogleLoginButton/>
                <Grid container direction={'row-reverse'}>
                    <Grid item>
                        <Link component={RouterLink} to={toLocation} variant="body2">
                            {helperLinkName}
                        </Link>
                    </Grid>
                </Grid>
            </Stack>
        </Stack>
    )
}

export default FormRegister;