import React, {useState} from 'react';
import {Button, IconButton, Modal, Paper, Stack} from "@mui/material";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import {useDispatch, useSelector} from "react-redux";
import Grid from "@mui/material/Grid";
import ClearIcon from '@mui/icons-material/Clear';
import {inviteDelete} from "../../store/actions/eventsActions";

const styleInviteForm = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '100%',
    maxWidth: '800px',
    bgcolor: 'whitesmoke',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const InvitationsModalBtn = () => {
    const dispatch = useDispatch();
    const [isOpenModal, setIsOpenModal] = useState(false);
    const events = useSelector((state) => state.events.data);
    const profile = useSelector((state) => state.profile.data);
    const loading = useSelector((state) => state.events.inviteDeleteLoading);

    const openModal = () => setIsOpenModal(true);
    const closeModal = () => setIsOpenModal(false);

    const inviteDeleteHandler = (data) => {
        dispatch(inviteDelete(data));
    }

    return (
        <>
            <Button onClick={openModal} variant={"contained"}>Invitations</Button>
            <Modal
                open={isOpenModal}
                onClose={closeModal}
            >
                <Box sx={styleInviteForm}>
                    {
                        events?.length > 0 && (
                            events.map((event) => {
                                return (
                                    <Stack component={Paper} my={2} p={1} key={event._id + 'inviteData'}>
                                        <Typography variant="h6" component="h2" textAlign={"center"}>
                                            Event :
                                        </Typography>
                                        <Grid container justifyContent={"space-between"} spacing={1}
                                              mb={2} alignItems={"center"} minHeight={'70px'}>
                                            <Grid item xs={4}>
                                                <Typography variant={'body1'}>
                                                    <strong>{event.date}</strong>
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={6}>
                                                <Typography variant={'body1'}>
                                                    {event.message}
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={2}>
                                                <Typography variant={'subtitle1'}>
                                                    {event.duration} hours
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                        <Typography variant="h6" component="h2" textAlign={"center"}>
                                            Invitations :
                                        </Typography>
                                        {event.invitations.map((email, count) => {
                                            if (email === profile.email) {
                                                return null;
                                            }
                                            return (
                                                <Grid container alignItems={"center"} spacing={1}
                                                      key={event._id + 'invite' + count} justifyContent={"center"}>
                                                    <Grid item>
                                                        {email}
                                                    </Grid>
                                                    <Grid item>
                                                        <IconButton onClick={() => {
                                                            inviteDeleteHandler({
                                                                event_id: event._id,
                                                                email,
                                                            });
                                                        }}
                                                                    color={'error'}
                                                                    disabled={loading === email}
                                                        >
                                                            <ClearIcon/>
                                                        </IconButton>
                                                    </Grid>
                                                </Grid>
                                            )
                                        })}
                                    </Stack>
                                )
                            })
                        )
                    }
                </Box>
            </Modal>
        </>
    );
};

export default InvitationsModalBtn;