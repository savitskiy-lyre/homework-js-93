const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Event = require("./models/Event");
const Moment = require("moment");


const run = async () => {
    await mongoose.connect(config.db.testStorageUrl)

    const collections = await mongoose.connection.db.listCollections().toArray();
    for (const collection of collections) {
        await mongoose.connection.db.dropCollection(collection.name);
    }

    const [user1, user2, user3] = await User.create({
        username: 'tester',
        password: 'test',
        email: 'user@good.com',
        image: '/fixtures/user.svg',
        token: nanoid(),
    }, {
        username: 'tester1',
        password: 'test',
        email: 'user1@good.com',
        image: '/fixtures/user.svg',
        token: nanoid(),
    }, {
        username: 'moder',
        password: '123',
        email: 'moder@good.com',
        image: '/fixtures/moder.jpg',
        role: 'moderator',
        token: nanoid(),
    })
    const [event1, event2, event3] = await Event.create({
        author: user1,
        date: new Moment()
            .add(Math.floor(Math.random() * 100), 'days')
            .add(Math.floor(Math.random() * 10), 'M')
            .add(Math.floor(Math.random() * 10), 'years')
            .format('YYYY.MM.DD | h:mm:ss a'),
        duration: (Math.random() * 8).toFixed(2),
        message: 'something happened1',
        invitations: [user1.email, user3.email],
    }, {
        author: user1,
        date: new Moment()
            .add(Math.floor(Math.random() * 100), 'days')
            .add(Math.floor(Math.random() * 10), 'mouths')
            .add(Math.floor(Math.random() * 10), 'years')
            .format('YYYY.MM.DD | h:mm:ss a'),
        duration: (Math.random() * 8).toFixed(2),
        message: 'something happened2',
        invitations: [user1.email, user2.email],
    }, {
        author: user2,
        date: new Moment()
            .add(Math.floor(Math.random() * 100), 'days')
            .add(Math.floor(Math.random() * 10), 'mouths')
            .add(Math.floor(Math.random() * 10), 'years')
            .format('YYYY.MM.DD | h:mm:ss a'),
        duration: (Math.random() * 8).toFixed(2),
        message: 'something happened3',
        invitations: [user2.email, user3.email],
    })

    await mongoose.connection.close();
}
run().catch(console.error)
