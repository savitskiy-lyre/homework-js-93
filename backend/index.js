const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const config = require('./config');
const users = require('./routers/users');
const events = require('./routers/events');

const server = express();
server.use(express.json());
server.use(express.static('public'));
server.use(cors());
const port = 9000;

server.get('/', (req, res) => {
    res.send('Hello')
})

server.use('/users', users);
server.use('/events', events);

const run = async () => {
    await mongoose.connect(config.db.testStorageUrl);

    server.listen(port, () => {
        console.log(`Server is started on ${port} port !`);
    })

    exitHook(() => {
        console.log('exiting');
        mongoose.disconnect();
    });
}
run().catch(e => console.error(e));