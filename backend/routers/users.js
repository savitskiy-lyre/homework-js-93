const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const authUser = require('../middleware/authUser');
const User = require('../models/User');
const {nanoid} = require("nanoid");
const config = require("../config");
const {OAuth2Client} = require('google-auth-library')
const clientGoogle = new OAuth2Client(process.env.GOOGLE_CLIENT_ID)

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.post('/', upload.single('image'), async (req, res) => {
    const {username, password, email} = req.body;
    const preUserData = {username, password, email};
    if (req.file) preUserData.image = '/uploads/' + req.file.filename;
    const user = new User(preUserData);

    try {
        user.generateToken();
        await user.save();
        res.send(user);

    } catch (err) {
        if (err?.errors) return res.status(400).send(err.errors);
        res.status(500).send({global: 'Server error, please try again !'});
    }
});

router.post('/googleLogin', async (req, res) => {
    const {token} = req.body;
    try {
        const ticket = await clientGoogle.verifyIdToken({
            idToken: token,
            audience: process.env.GOOGLE_CLIENT_ID,
        });
        const {name, email, picture} = ticket.getPayload();

        let  user = await User.findOne({email});

        if (!user) {
            user = new User({
                email,
                username: name,
                password: nanoid(),
                image: picture || null,
            })
        }

        user.generateToken();
        await user.save({validateBeforeSave: false});

        res.send(user);
    } catch (err) {
        res.status(500).send({global: 'Server error, please try again !'});
    }
});

router.post('/sessions', authUser, async (req, res) => {
    try {
        req.user.generateToken();
        await req.user.save({validateBeforeSave: false});
        res.send(req.user);
    } catch (err) {
        res.status(500).send({global: 'Server error, please try again !'});
    }

});

router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Not really Success'};
    if (!token) return res.send({message: 'No token Success'});
    const user = await User.findOne({token});
    if (!user) return res.send(success);
    try {
        user.generateToken();
        await user.save({validateBeforeSave: false});
        return res.send({message: 'Success'});
    } catch (err) {
        res.status(500).send({global: 'Server error, please try again !'});
    }

});

module.exports = router;