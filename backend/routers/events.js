const express = require('express');
const authUserToken = require("../middleware/authUserToken");
const router = express.Router();
const Event = require('../models/Event');
const User = require("../models/User");


router.get('/', authUserToken, async (req, res) => {
    try {
        const events = await Event
            .find({invitations: {$in: req.user.email}})
            .populate('author')
            .sort('date');

        res.send(events);
    } catch (err) {
        res.status(500).send({global: 'Internal Server Error'})
    }
});
router.post('/', authUserToken, async (req, res) => {
    const {date, duration, message} = req.body;
    const event = new Event({date, duration, message, author: req.user, invitations: req.user.email});
    try {
        await event.save();
        res.send(event);
    } catch (err) {
        if (err?.errors) return res.status(400).send(err.errors);
        res.status(500).send({global: 'Internal Server Error'})
    }
});
router.post('/invite', authUserToken, async (req, res) => {
    const {event_id, email} = req.body;
    if (!event_id || !email) return res.status(400).send({global: 'Event id and email is required to create invite !'})
    try {
        const eventsData = await Promise.all([Event.findById(event_id), User.findOne({email})]);
        const [event, linkedUser] = eventsData;
        if (!event) return res.status(404).send({global: 'Not found event'});
        if (!linkedUser) return res.status(404).send({global: 'Not found linked user'});
        if (event.author.toString() !== req.user._id.toString()) {
            return res.status(403).send({global: 'You are not allowed to create an invite not to your event'});
        }
        if (event.invitations.find(e => e === linkedUser.email)) {
            return res.send({global: 'The invitation already exists'});
        }
        event.invitations.push(linkedUser.email);
        event.save();
        res.send({global: 'Invite successful created !'});
    } catch (err) {
        console.log(err);
        res.status(500).send({global: 'Internal Server Error'})
    }
});
router.delete('/invite', authUserToken, async (req, res) => {
    const {event_id, email} = req.body;
    if (!event_id || !email) return res.status(400).send({global: 'Event id and email is required to create invite !'})
    try {
        const eventsData = await Promise.all([Event.findById(event_id), User.findOne({email})]);
        const [event, linkedUser] = eventsData;
        if (!event) return res.status(404).send({global: 'Not found event'});
        if (!linkedUser) return res.status(404).send({global: 'Not found linked user'});
        if (event.author.toString() !== req.user._id.toString()) {
            return res.status(403).send({global: 'You are not allowed to delete invite not to your event'});
        }
        if (!event.invitations.find(e => e === linkedUser.email)) {
            return res.status(404).send({global: 'Invite not found'});
        }
        event.invitations = event.invitations.filter(e => e !== linkedUser.email);
        event.save();
        res.send({global: 'Invite successful deleted !'});
    } catch (err) {
        res.status(500).send({global: 'Internal Server Error'})
    }
});
router.delete('/', authUserToken, async (req, res) => {
    const {event_id} = req.body;
    try {
        const event = await Event.findById(event_id);
        if (!event) return res.status(404).send({global: 'Event not found'});
        if (event.author.toString() !== req.user._id.toString()) return res.status(403).send({global: 'You are not allowed to delete not your event'});
        await Event.findByIdAndDelete(event_id);
        res.send({global: 'Event successful deleted'});
    } catch (err) {
        res.status(500).send({global: 'Internal Server Error'})
    }
});

module.exports = router;