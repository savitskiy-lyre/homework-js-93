const path = require('path');

const rootPath = __dirname;

module.exports = {
   rootPath,
   uploadPath: path.join(rootPath, 'public/uploads'),
   db: {
      testStorageUrl: 'mongodb://localhost/testStorage',
   },
};