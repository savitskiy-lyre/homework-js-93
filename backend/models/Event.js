const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const EventSchema = new mongoose.Schema({
    author: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'Author is required'],
        ref: 'users',
    },
    date: {
        type: String,
        required: [true, 'Date is required'],
    },
    duration: {
        type: Number,
        required: [true, 'Duration field is required'],
    },
    message: {
        type: String,
        required: [true, 'Message is required']
    },
    invitations: {
        type: [String],
    },
})

EventSchema.plugin(idValidator);
const Event = mongoose.model('events', EventSchema);
module.exports = Event;